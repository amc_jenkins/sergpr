# fix stupid using static version of gem. Only for RM 2.6.
if Redmine::VERSION::MAJOR == 2 && Redmine::VERSION::MINOR == 6
  module CollectiveIdea::Acts::NestedSet::Model
    def nested_set_scope(options = {})
      if (scopes = Array(acts_as_nested_set_options[:scope])).any?
        options[:conditions] = scopes.inject({}) do |conditions,attr|
          conditions.merge attr => self[attr]
        end
      end

      self.class.base_class.unscoped.nested_set_scope options
    end
  end
end
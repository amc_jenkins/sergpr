class CmItemCustom < CmItem

  validates :caption, presence: true

  def item_text(view, opts, *args)
    cl = opts.delete(:class)
    opts = (self.options || {}).merge(opts)
    opts[:class] = opts[:class].to_s + ' ' + cl if cl.present?

    self.custom_url ? view.link_to("<span>#{self.caption}</span>".html_safe, self.custom_url || '#', opts) : "<span>#{self.custom_url}</span>".html_safe
  end

  def self.item_label(code)
    ''
  end

  def item_label
    self.caption
  end

  def can_edit?
    User.current.admin?
  end

  def visible?
    (!self.logged || User.current.logged?) && (!self.admin || User.current.admin?) && (self.parent.blank? || self.parent.visible?)
  end
end